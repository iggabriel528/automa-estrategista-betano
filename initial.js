const TelegramChat = require("./app/models/chat");
const Monitor = require("./app/models/monitor");
const RouletteParam = require("./app/models/rouletteparam");
const Adm = require("./app/models/adm");
const bcryptjs = require("bcryptjs");
const Socket = require("./app/models/socket");
const Message = require("./app/models/message");

(async()=>{
    var newadm = false;
    var adm = { _id: undefined };
    if(newadm){
        adm = await Adm.create({name:'Igor Gabriel',email:'ig.gabriel@outlook.com',password:bcryptjs.hashSync('@IG552288or*',10)});
    }else{
        adm._id = '6222503068d9829326ba2cb4';
    }
    const monitor = await Monitor.create({user_id:adm._id,name:'Novo BOT',active:false,game:'betano_evolution'});
    if(monitor){
        const roulettes = [
            'Brazilian Roulette', 'Lightning Roulette', 'Roulette', 'Immersive Roulette',
            'Auto-Roulette', 'Speed Roulette', 'VIP Roulette', 'French Roulette Gold', 
            'American Roulette', 'Speed Auto Roulette', 'Auto-Roulette VIP', 
            'Auto-Roulette La Partage', 'Salon Privé Roulette', 'Deutsches Roulette',
            'Türkçe Rulet', 'Bucharest Roulette', 'Casino Malta Roulette'
        ]
        const strategies = [
            {strategy:'repetition',substrategies:['color','oddev','hilow','column','dozen']},
            {strategy:'absense',substrategies:['column','dozen']},
            {strategy:'alternation',substrategies:['color','oddev','hilow']}
        ]
        for(const roulette of roulettes){
            for(const strategy of strategies){
                for(const substrategy of strategy.substrategies){
                    await RouletteParam.create({
                        monitor_id:monitor._id,
                        roulette,
                        strategy:strategy.strategy,
                        substrategy:substrategy,
                        param:6,
                        cover_zero:true,
                        active:true,
                        attempts:3
                    })   
                }
            }
        }
        const messages = [
            {category:'warning',lines:[
                {line:'title',present:true,message:'Possível sinal'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '},
                {line:'links',present:false,message:'Links: '}
            ]},
            {category:'signal',lines:[
                {line:'title',present:true,message:'Sinal confirmado'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '},
                {line:'enter',present:true,message:'Entrada: '},
                {line:'after',present:true,message:'Depois de: '},
                {line:'cover_zero',present:true,message:'Cobrir zero'},
                {line:'links',present:false,message:'Links: '}
            ]},
            {category:'result',lines:[
                {line:'green',present:true,message:'GREEN'},
                {line:'red',present:true,message:'RED'},
                {line:'result',present:true,message:'**'}
            ]},
            {category:'edit',lines:[
                {line:'title',present:true,message:'Sinal finalizado'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '}
            ]},
        ]
        for(const message of messages){
            for(const line of message.lines){
                await Message.create({monitor_id:monitor._id,category:message.category,line:line.line,present:line.present,message:line.message});
            }
        }
        await Socket.create({monitor_id:monitor._id});
        await TelegramChat.create({monitor_id:monitor._id})
        console.log('Processo de criação concluído');
    }
})();