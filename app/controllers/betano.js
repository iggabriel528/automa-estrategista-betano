const puppeteer = require("puppeteer");
const { log, logerror } = require("../tools");
const evolution = require("./evolution");

async function init(){
    const browser = await puppeteer.launch({headless:true,args:["--force-device-scale-factor=0.5"]});
    const login = await browser.newPage();
    log('Acessando área de login')
    await login.goto('https://br.betano.com/myaccount/login');
    await login.waitForSelector('[id="username"]').then(async()=>{
        log('Área de login carregada, efetuando acesso');
        const usernameinput = await login.$('[id="username"]');
        const usernameinputpos = await usernameinput.boundingBox();
        await login.mouse.click((usernameinputpos.x + (usernameinputpos.width/2)),(usernameinputpos.y + (usernameinputpos.height/2)));
        await login.keyboard.type('ig.gabriel@outlook.com',{delay:20});
        const passwordinput = await login.$('[id="password"]');
        const passwordinputpos = await passwordinput.boundingBox();
        await login.mouse.click((passwordinputpos.x + (passwordinputpos.width/2)),(passwordinputpos.y + (passwordinputpos.height/2)));
        await login.keyboard.type('@IG552288or*',{delay:20});
        await login.keyboard.press('Enter');
        await login.waitForSelector('[class*="balance-summary__value"]').then(async()=>{
            log('Logado na conta, carregando lobbys');
            evolution(browser);
        }).catch(error=>{
            logerror('LOGIN: Seletor userbalance não localizado');
        })
    }).catch(error=>{
        logerror('LOGIN: Seletor username não localizado');
    })
}

module.exports = init;