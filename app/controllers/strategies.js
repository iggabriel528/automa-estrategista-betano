const { Numbers } = require("../constants/numbers");
const monitor = require("./monitor");

const strategies = {
    repetition:async(data,params)=>{
        const { monitor_id, substrategy, param, attempts, cover_zero } = params;
        const { roulette, results, game } = data;
        
        try {
            const substrategies = {
                color:{
                    descript:'Repetição de ',
                    _descript:{1:'números vermelhos',2:'números pretos'},
                    descript_enter:{1:'Números pretos',2:'Números vermelhos'},
                    _need_green:{1:[2],2:[1]}
                },hilow:{
                    descript:'Repetição de ',
                    _descript:{1:'números baixos',2:'números altos'},
                    descript_enter:{1:'Números altos',2:'Números baixos'},
                    _need_green:{1:[2],2:[1]}
                },oddev:{
                    descript:'Repetição de ',
                    _descript:{1:'números ímpares',2:'números pares'},
                    descript_enter:{1:'Números pares',2:'Números ímpares'},
                    _need_green:{1:[2],2:[1]}
                },column:{
                    descript:'Repetição da ',
                    _descript:{1:'1ª coluna',2:'2ª coluna',3:'3ª coluna'},
                    descript_enter:{1:'2ª e 3ª colunas',2:'1ª e 3ª colunas',3:'1ª e 2ª colunas'},
                    _need_green:{1:[2,3],2:[1,3],3:[1,2]}
                },dozen:{
                    descript:'Repetição da ',
                    _descript:{1:'1ª dúzia',2:'2ª dúzia',3:'3ª dúzia'},
                    descript_enter:{1:'2ª e 3ª dúzias',2:'1ª e 3ª dúzias',3:'1ª e 2ª dúzias'},
                    _need_green:{1:[2,3],2:[1,3],3:[1,2]}
                }
            };

            const { descript, _descript, descript_enter, _need_green } = substrategies[substrategy];
            if(results.length > param-1){
                var valid = true;
                const initial = parseInt(Numbers[parseInt(results[0])][substrategy]);
                for(var r=0;r<param-1;r++){
                    if(results[r]==0){
                        valid = false;
                    }else{
                        if(parseInt(Numbers[parseInt(results[r])][substrategy])!=initial){
                            valid = false;
                        }
                    }
                }

                if(parseInt(Numbers[parseInt(results[param-1])][substrategy])==initial){
                    valid = false;
                }

                var resultstovalidade = [];
                for(var r=0;r<param-1;r++){
                    resultstovalidade.push(Numbers[results[r]][substrategy])
                }

                resultstovalidade = resultstovalidade.filter((e)=>{return e!=initial});
            
                if(resultstovalidade.length>0){valid = false};

                if(valid){
                    monitor({
                        monitor_id,
                        game,
                        roulette,//Nome da roleta que está sendo analisada
                        strategy:'repetition',//Estratégia que está sendo utilizada
                        substrategy,//Substratégia (colunas, dúzias, cores, pares e ímpares, altos e baixos)
                        results,//Resultados computados no momento da primeira análise
                        descript:`${descript}${_descript[initial]}`,//Descrição para apresentação na tela
                        descript_enter:`${descript_enter[initial]}`,//Descrição da entrada
                        need_signal:[initial],//Resultado necessário para conversão do aviso em sinal
                        need_green:_need_green[initial],//Resultado necessário para que a aposta seja vencedora
                        attempts,//Quantidade máxima de tentativas permitidas
                        cover_zero,//Cobertura do zero na aposta
                        param,
                        item:initial
                    })
                }
            }
        } catch (error) {
            console.log(`${roulette} :: ${error} :: ${results.join(' | ')}`);
        }
    },
    alternation:async(data,params)=>{
        
    },
    absense:async(data,params)=>{
        try {
            const { monitor_id, substrategy, param, attempts, cover_zero } = params;
            const { roulette, results, game } = data;
    
            const substrategies = {
                column:{
                    descript:'Ausência da ',
                    _descript:{
                        1:'1ª coluna',2:'2ª coluna',3:'3ª coluna'
                    },
                    _need_signal:{
                        0:[1,2,3],1:[2,3],2:[1,3],3:[1,2]
                    }
                },
                dozen:{
                    descript:'Ausência da ',
                    _descript:{
                        1:'1ª dúzia',2:'2ª dúzia',3:'3ª dúzia'
                    },
                    _need_signal:{
                        0:[1,2,3],1:[2,3],2:[1,3],3:[1,2]
                    }
                }
            };
            const { descript, _descript, _need_signal } = substrategies[substrategy];
                if(results.length>param-1){
                    var initial = parseInt(Numbers[results[0]][substrategy]);
                    var falses = _need_signal[initial];
                    var poss = [];
                    for(var a in falses){
                        var valid = true;
                        for(var b=0;b<param-1;b++){
                            if(parseInt(Numbers[parseInt(results[b])][substrategy])==falses[a] || results[b]==0){
                                valid = false;
                            }
                        }
                        if(falses.indexOf(Numbers[parseInt(results[param-1])][substrategy])>-1){
                            valid = false;
                        }
                        if(valid){
                            poss.push(falses[a]);
                        }
                    }
                    if(poss.length==1){
                        var resultstovalidade = [];
                        for(var r=0;r<param-1;r++){
                            resultstovalidade.push(Numbers[results[r]][substrategy]);
                        }
    
                        resultstovalidade = resultstovalidade.filter((e)=>{return e==poss[0]});
                        if(resultstovalidade.length==0){
                            monitor({
                                monitor_id,
                                game,
                                roulette,//Nome da roleta que está sendo analisada
                                strategy:'absense',//Estratégia que está sendo utilizada
                                substrategy,//Substratégia (colunas, dúzias, cores, pares e ímpares, altos e baixos)
                                results,//Resultados computados no momento da primeira análise
                                descript:`${descript}${_descript[poss[0]]}`,//Descrição para apresentação na tela
                                descript_enter:`${_descript[poss[0]]}`,//Descrição da entrada
                                need_signal:_need_signal[poss[0]],//Resultado necessário para conversão do aviso em sinal
                                need_green:[poss[0]],//Resultado necessário para que a aposta seja vencedora
                                attempts,//Quantidade máxima de tentativas permitidas
                                cover_zero,//Cobertura do zero na aposta
                                param:param,
                                item:poss[0]
                            });
                        }
                    }
                }
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = strategies;