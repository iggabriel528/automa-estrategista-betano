const { logerror, sleep, log } = require("../tools");
const {analisys} = require("./analisys");
const { queryRoulette, addRoulette, updateRoulette } = require("./roulettes");
const moment = require('moment');

async function evolution(browser){
    const page = await browser.newPage();
    await page.setViewport({width:2000,height:2000})
    await page.goto('https://br.betano.com/casino/live/games/brazilian-roulette/3136/tables/');
    await sleep(3000);
    await page.waitForSelector('[class="game-play-providers"]').then(async()=>{
        var src = await page.evaluate(()=>{return document.querySelector('[class="game-play-providers"]').getAttribute('src')});
        await page.goto(src);
        var lastrefresh = parseInt(moment().format('x'));
        loadLobby();
        async function loadLobby(){
            await page.goto('https://betano-br.evo-games.com/frontend/evo/r2/#game=roulette');
            await page.waitForSelector('[data-role="table-block"]').then(async()=>{
                var timer = setInterval(async()=>{
                    var actualtimer = parseInt(moment().format('x'));
                    if(actualtimer-lastrefresh > 120000){
                        clearInterval(timer);
                        log(`Evolution :: Recarregando lobby`);
                        lastrefresh = actualtimer;
                        loadLobby();
                    }else{
                        var roulettes = await page.evaluate(()=>{
                            var tables_to_return = [];
                            var tables = document.querySelectorAll('[data-role="table-block"]');
                            for(var table of tables){
                                const name = table.querySelector('[data-role="table-name"]');
                                const results = table.querySelector('[data-role="roulette-statistics-container"]');
                                if(name && results){
                                    if(name.innerText.indexOf('Double')==-1 && name.innerText.indexOf('French')==-1){
                                        var arrayresults = [];
                                        var divresults = results.innerText.split('\n');
                                        for(var divresult of divresults){
                                            if(divresult.indexOf('x')==-1){
                                                arrayresults.push(parseInt(divresult));
                                            }
                                        }
                                        tables_to_return.push({
                                            roulette:name.innerText,
                                            results:arrayresults,
                                            game:'betano_evolution'
                                        })
                                    }
                                }
                            }
                            return tables_to_return;
                        });
                        if(roulettes.length>0){
                            for(var roulette of roulettes){
                                if(queryRoulette(roulette)==undefined){
                                    log(`Evolution: Iniciando roleta ${roulette.roulette}`);
                                    addRoulette(roulette);
                                    analisys(roulette);
                                }else{
                                    var roulettesaved = queryRoulette(roulette);
                                    var diference = false;
                                    for(var i=0;i<10;i++){
                                        if(roulettesaved.results[i]!=roulette.results[i]){
                                            diference = true;
                                        }
                                    }
                                    if(diference){
                                        roulettesaved.results.reverse();
                                        roulettesaved.results.push(roulette.results[0]);
                                        roulettesaved.results.reverse();
                                        var valid = true;
                                        for(var i=0;i<10;i++){
                                            if(roulettesaved.results[i]!=roulette.results[i]){
                                                valid = false;
                                            }
                                        }
                                        if(valid){
                                            log(`Evolution :: ${roulette.roulette} :: ${roulette.results[0]}`);
                                            updateRoulette(roulettesaved);
                                            analisys(roulettesaved);
                                        }else{
                                            log(`Evolution :: ${roulette.roulette} :: Reiniciada por inconsistência`);
                                            updateRoulette(roulette);
                                            analisys(roulette);
                                        }
                                    }
                                }
                            }
                        }
                    }
                },1000);
            }).catch(error=>{
                logerror('Erro ao carregar lobby evolution, reiniciando');
                evolution(browser);
            });
        }
    });
}

module.exports = evolution;