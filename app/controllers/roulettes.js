const Roulette = require("../models/roulette");
const moment = require('moment');
var roulettes = [];

async function addRoulette(data){
    const { game, roulette, results } = data;
    roulettes.push(data);
    var roulettesaved = await Roulette.find({game, roulette});
    if(roulettesaved.length==0){
        await Roulette.create(data);
    }else{
        await Roulette.updateOne({game,roulette},{results, last_update:moment().format('YYYY-MM-DD HH:mm:ss')});
    }
}

function queryRoulette(roulette){
    for(var roul of roulettes){
        if(roul.roulette==roulette.roulette && roul.game==roulette.game){
            return roul;
        }
    }
    return undefined;
}

async function updateRoulette(data){
    const { roulette, game, results } = data;
    for(var i in roulettes){
        if(roulettes[i].roulette==roulette && roulettes[i].game==game){
            roulettes[i].results = results;
            await Roulette.updateOne({game,roulette},{results, last_update:moment().format('YYYY-MM-DD HH:mm:ss')});
        }
    }
}

module.exports = {addRoulette, queryRoulette, updateRoulette}