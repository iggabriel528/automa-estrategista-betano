const moment = require('moment');

function log(message){
    const datestring = moment().format('DD/MM HH:mm:ss');
    console.log(`${datestring} :: ${message}`);
}

function logerror(message){
    const datestring = moment().format('DD/MM HH:mm:ss');
    console.log(`${datestring} :: ${message}`);
}

async function sleep(ms){
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

module.exports = { log, logerror, sleep }