const mongoose = require('../database/index');
    
const SocketSchema = new mongoose.Schema({
    monitor_id:{type:String,required:true},
    name:{type: String,required: true, default:'Novo SOCKET'},
    active:{type:Boolean,required:true,default:true},
    app_id:{type:String,required:true,default:'...'},
    key:{type:String,required:true,default:'...'},
    secret:{type:String,required:true,default:'...'},
    cluster:{type:String,required:true,default:'...'},
    created_at:{type: Date,default: Date.now}
});

const Socket = mongoose.model('Socket',SocketSchema);
module.exports = Socket;
