const mongoose = require('../database/index');
    
const MonitorSchema = new mongoose.Schema({
    user_id:{type:String,required:true},
    name:{type: String,required: true},
    active:{type:Boolean,required:true,default:false},
    active_adm:{type:Boolean,required:true,default:false},
    game:{type:String,required:true},
    created_at:{type: Date,default: Date.now}
});

const Monitor = mongoose.model('Monitor',MonitorSchema);
module.exports = Monitor;
