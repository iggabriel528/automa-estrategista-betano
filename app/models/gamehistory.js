const mongoose = require('../database/index');
    
const GameHistorySchema = new mongoose.Schema({
    game:{type: String, required:true},
    name:{type: String, required: true},
    result:{type: String, required:true},
    created_at:{type: Date,default: Date.now}
});

const GameHistory = mongoose.model('GameHistory',GameHistorySchema);
module.exports = GameHistory;
