const mongoose = require('../database/index');
    
const RouletteSchema = new mongoose.Schema({
    game:{type:String,required:true},
    roulette:{type: String,required: true},
    results:{type:Array,required:true,default:[]},
    last_update:{type:Date,required:true,default:Date.now},
    created_at:{type: Date,default: Date.now}
});

const Roulette = mongoose.model('Roulette',RouletteSchema);
module.exports = Roulette;
